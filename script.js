let trainer = {
	name: "Ash Kechum",
	age: 10,
	friends: {
		hoenn: ["Max", "May"],
		kanto: ["Brock", "Misty"],
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],

	talk: function() {
		console.log(`${this.pokemon[0]}! I choose you!`);
	}
}
console.log(trainer);
console.log(trainer.name);

console.log(`Result of square bracket notation`);
console.log(trainer['pokemon']);

console.log(`Result of talk method`);
console.log(trainer.talk());

function pokemon(name, lvl, hp, lvl){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;

	this.tackle = function(target) {
		let remainingHp = target.health - this.attack;

		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to ${remainingHp}`);

		if(remainingHp <= 0) {
			faint();
		}

		function faint(){
			console.log(`${target.name} fainted`);
		}

		target.health = remainingHp;
	}
}
let pikachu = new pokemon("Pikachu", 12, 12, 12);
console.log(pikachu);

let geodude = new pokemon("Geodude", 8, 8, 8);
console.log(geodude);

let mewtwo = new pokemon("Mewtwo", 100, 100, 100);
console.log(mewtwo);

console.log(geodude.tackle(pikachu));

console.log(pikachu);

console.log(mewtwo.tackle(geodude));
console.log(geodude);
